# terraform {
#   }
# }



# terraform {
#   backend "http" {
#     # address = "https://gitlab.com/api/v4/projects/53009140/terraform/state/prod"
#     # lock_address = "https://gitlab.com/api/v4/projects/53009140/terraform/state/prod/lock"
#     # unlock_address = "https://gitlab.com/api/v4/projects/53009140/terraform/state/prod/lock"
#     # address = "https://gitlab.com/api/v4/projects/53009140/terraform/state/terraform.tfstate"
#     username = "randallalala1"
#     password = var.password
#     lock_method = "POST"
#     unlock_method = "DELETE"
#     retry_max = "3"
#     # authentication {
#     #   token = "{{ CI_JOB_TOKEN }}"
#     # }
#   }
#   required_providers{
#     gitlab = {
#       source = "gitlabhq/gitlab"
#       version= "~> 3.1"
#     }
#   }
# }

provider "aws" {
  region  = "ap-southeast-1"
  profile = "randallalala"

  # access_key = "YOUR_ACCESS_KEY"
  # secret_key = "YOUR_SECRET_KEY"

}


provider "gitlab" {
  token = var.gitlab_access_token
}

data "gitlab_project" "visitor_counter" {
  id = 53009140
}

resource "gitlab_project_variable" "visitor_counter_variable" {
  project = data.gitlab_project.visitor_counter.id
  key = "var"
  value = "hi"
}



# terraform {
#   backend "http" {
#     # address        = "https://gitlab.com/api/v4/projects/53009140/terraform"
#     # lock_address        = "https://gitlab.com/api/v4/projects/53009140/terraform"
#     # unlock_address        = "https://gitlab.com/api/v4/projects/53009140/terraform"

#   }
# }

resource "aws_s3_bucket" "visitorbucket" {
  bucket = "visitorbucket"
  # acl    = "private"

  tags = {
    Name        = "visitorbucket"
    Environment = "Dev"
  }
}


# terraform {
#   backend "remote" {
#     organization = "self"

#     workspaces {
#       name = "x"
#     }
#   }
# }



# terraform {
#   backend "terraform_remote_state" {
#     config = {
#       hostname = "gitlab.com"
#       organization = "YOUR_GITLAB_ORGANIZATION_NAME"
#       project = "YOUR_GITLAB_PROJECT_ID"
#       path = "terraform/state.tfstate"
#       token = "YOUR_PERSONAL_ACCESS_TOKEN"
#     }
#   }
# }



# Create VPC and subnets
resource "aws_vpc" "main" {
  # source = "modules/ec2"

  cidr_block = "10.0.0.0/16"
}

data "aws_ami" "eks_worker" {
  filter {
    name   = "name"
    values = ["amazon-eks-*"]
  }
  most_recent = true
  owners      = ["602401143452"] # Amazon EKS AMI account ID
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "eks-igw"
  }
}

resource "aws_subnet" "public" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = true
  tags = {
    Name = "eks-public-subnet"
  }
}

resource "aws_subnet" "private" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "ap-southeast-1b"
  tags = {
    Name = "eks-private-subnet"
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = var.pubkey
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "eks" {
  name               = "eks-cluster-eks"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_role_policy_attachment" "eks-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks.name
}

resource "aws_iam_role_policy_attachment" "eks-AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.eks.name
}

resource "aws_eks_cluster" "ekstest" {
  name     = "ekstest"
  role_arn = aws_iam_role.eks.arn

  vpc_config {
    subnet_ids = [aws_subnet.private.id, aws_subnet.public.id]
  }

  depends_on = [
    aws_iam_role_policy_attachment.eks-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.eks-AmazonEKSVPCResourceController,
  ]
}

output "endpoint" {
  value = aws_eks_cluster.ekstest.endpoint
}

# output "kubeconfig-certificate-authority-data" {
#   value = aws_eks_cluster.eks.certificate_authority[0].data
# }


# module "eks" {
#   source          = "terraform-aws-modules/eks/aws"
#   cluster_name    = "my-eks-cluster"
#   cluster_version = "1.20"
#   subnets         = [aws_subnet.public.id, aws_subnet.private.id]
#   vpc_id          = aws_vpc.main.id

#   node_groups = {
#     eks_nodes = {
#       desired_capacity = 2
#       max_capacity     = 10
#       min_capacity     = 1

#       instance_type = "m5.large"
#       key_name      = "my-key-name"

#       ami_id = data.aws_ami.eks_worker.id

#       additional_tags = {
#         Environment = "test"
#         Name        = "eks-worker-node"
#       }
#     }
#   }
# }


# Create security groups
resource "aws_security_group" "worker_node" {
  name   = "eks-worker-node-sg"
  vpc_id = aws_vpc.main.id
  ingress = [
    {
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = []
      description      = "Allow SSH access from anywhere"
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    },
    {
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = []
      description      = "Allow HTTP access from anywhere"
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    },
    {
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = []
      description      = "Allow HTTPS access from anywhere"
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "redis" {
  name        = "redis_sg"
  description = "Security group for Redis instance"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}



# # Create EC2 Launch Template
# resource "aws_launch_template" "web_server" {
#   name = "web_server_template"
#   # image_id = var.ami
#   image_id = "ami-03b6c12f592f54cf3"
#   instance_type = var.instance_type
#   vpc_security_group_ids = [aws_security_group.worker_node.id]
#   iam_instance_profile {
#     name = aws_iam_instance_profile.web_server.name
#   }
#   user_data = base64encode(<<EOF
# #!/bin/bash
# echo "Hello, World!" > index.html
# nohup busybox httpd -f -p 8080 &
# EOF
#   )
# }


# Create Auto Scaling Group
# resource "aws_autoscaling_group" "web_server" {
#   name = "web_server_asg"
#   max_size = 3
#   min_size = 2
#   desired_capacity = 2
#   health_check_grace_period = 300
#   health_check_type = "ELB"
#   launch_template {
#     name = aws_launch_template.web_server.id
#     version = "$Latest"
#   }
#   vpc_zone_identifier = tolist(aws_subnet.public[*].id)
#   depends_on = [aws_subnet.public]
# }

# resource "aws_instance" "elb" {
#   ami           = var.ami
#   instance_type = "t2.micro"
# }


# # Create Elastic Load Balancer
# resource "aws_elb" "web_server" {
#   name = "web-server-elb"
#   internal = false
#   availability_zones = var.availability_zones
#   listener {
#     instance_port     = 80
#     instance_protocol = "http"
#     lb_port           = 80
#     lb_protocol       = "http"
#   }
#   health_check {
#     healthy_threshold   = 2
#     unhealthy_threshold = 2
#     timeout             = 3
#     target              = "HTTP:80/"
#     interval            = 30
#   }
# }

